/*
 * Library to extract specific datafields from MARCXML records
 * Copyright (C) 2020  project swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.slsp

import scala.collection.mutable.ArrayBuffer
import scala.xml.{Elem, NodeSeq}

trait Subfield {
  def get(elem: Elem): NodeSeq
}

class SubfieldExtractor(extractor: Elem => (String, NodeSeq)) extends Subfield {
  override def get(elem: Elem): NodeSeq = {
    val (code, nodeSeq) = extractor(elem)
    //@formatter=off
    nodeSeq map (n => <subfield code={code}>{n.text}</subfield>)
    //@formatter=on
  }
}

class SubfieldLiteral(name: String, value: String) extends Subfield {
  //@formatter=off
  override def get(elem: Elem): NodeSeq = ArrayBuffer(<subfield code={name}>{value}</subfield>)

  //@formatter=on
}

/**
 * Builds a new field out of parts of an original record
 *
 * @param tag  Tag (field number) of new field
 * @param ind1 First indicator of new field
 * @param ind2 Second indicator of new field (defaults to None (i.e. blank indicator))
 */
case class FieldBuilder(tag: String, ind1: Elem => Option[String], ind2: Option[Elem => Option[String]] = None) {

  private val subfields: ArrayBuffer[Subfield] = ArrayBuffer()
  private var buildOnly: Option[String] = None

  /**
   * Build (extract) the record only if the indicated field exists
   *
   * @param field Field which must exist
   * @return This FieldBuilder instance
   */
  def buildOnlyIfFieldExists(field: String): FieldBuilder = {
    buildOnly = Some(field)
    this
  }

  /**
   * Register an extractor for a specific subfield's value
   *
   * @param field        Parent field tag (field number)
   * @param origSubfield Original subfield code
   * @param newSubfield  New Subfield code
   * @return This FieldBuilder instance
   */
  def registerSubfieldExtractor(field: String, origSubfield: String, newSubfield: String): FieldBuilder = {
    subfields append new SubfieldExtractor((e: Elem) => (newSubfield, getSubfield(e, field, origSubfield)))
    this
  }

  /**
   * Register an extractor for a specific subfield's value
   *
   * @param field    Parent field tag (field number)
   * @param subfield Subfield code
   * @return This FieldBuilder
   */
  def registerSubfieldExtractor(field: String, subfield: String): FieldBuilder = {
    registerSubfieldExtractor(field, subfield, subfield)
    this
  }

  /**
   * Register an extractor for a specific subfield's value. If the subfield does not exist, look for second subfield
   *
   * @param field                Parent field tag (field number)
   * @param origSubfield         Original subfield code
   * @param newSubfield          New subfield code
   * @param fallbackField        Fallback parent field tag (field number)
   * @param fallbackOrigSubfield Original fallback subfield code
   * @param fallbackNewSubfield  New fallback subfield code
   * @return This FieldBuilder
   */
  def registerSubfieldExtractor(field: String,
                                origSubfield: String,
                                newSubfield: String,
                                fallbackField: String,
                                fallbackOrigSubfield: String,
                                fallbackNewSubfield: String): FieldBuilder = {
    subfields append new SubfieldExtractor((e: Elem) => {
      val subfield = getSubfield(e, field, origSubfield)
      if (subfield.isEmpty) {
        (fallbackNewSubfield, getSubfield(e, fallbackField, fallbackOrigSubfield))
      } else {
        (newSubfield, subfield)
      }
    })
    this
  }

  /**
   * Register an extractor for a specific subfield's value. If the subfield does not exist, look for second subfield
   *
   * @param field            Parent field tag (field number)
   * @param subfield         Subfield code
   * @param fallbackField    Fallback parent field tag (field number)
   * @param fallbackSubfield Fallback subfield code
   * @return This FieldBuilder
   */
  def registerSubfieldExtractor(field: String,
                                subfield: String,
                                fallbackField: String,
                                fallbackSubfield: String): FieldBuilder = {
    registerSubfieldExtractor(field, subfield, subfield, fallbackField, fallbackSubfield, fallbackSubfield)
    this
  }

  def addSubfield(name: String, value: String): FieldBuilder = {
    if (!value.isEmpty) subfields append new SubfieldLiteral(name, value)
    this
  }

  /**
   * Build new field from original record
   *
   * @param rec Original record
   * @return New field
   */
  def buildFrom(rec: Elem): Option[Elem] = {
    val indicator1 = ind1(rec) match {
      case Some(i) => i
      case None => " "
    }
    val indicator2 = ind2 match {
      case Some(i) if indicator1 != " " => i(rec).get
      case _ => " "
    }
    val children: NodeSeq = subfields
      .map(_.get(rec))
      .reduce(_ ++ _)
    if (children.isEmpty || !has_field(rec, buildOnly)) {
      None
    } else {
      //@formatter=off
      val newField: Elem = <datafield tag={tag} ind1={indicator1} ind2={indicator2}></datafield>
      //@formatter=on
      Some(addChild(newField, children))
    }
  }

  private def getSubfield(rec: Elem, field: String, subfield: String): NodeSeq = {
    for {
      df <- rec \\ "datafield"
      if df \@ "tag" == field
      sf <- df \\ "subfield"
      if sf \@ "code" == subfield
    } yield sf
  }

  private def addChild(n: Elem, newChild: NodeSeq) = n match {
    case Elem(prefix, label, attribs, scope, child@_*) =>
      Elem(prefix, label, attribs, scope, false, child ++ newChild: _*)
    case _ => n
  }

  private def has_field(rec: Elem, field: Option[String]): Boolean = field match {
    case Some(f) => (for {
      df <- rec \\ "datafield"
      if df \@ "tag" == f
    } yield df).length > 0
    case None => true
  }
}

/**
 * Provides some basic selectors for indicator values
 */
object IndSelectors {
  /**
   * Return an empty indicator (as Option type)
   */
  val customInd: String => Elem => Option[String] = x => _ => Some(x)

  /**
   * Return value of indicator (as Option type)
   */
  val getInd: String => Int => Elem => Option[String] = tag => indPos => e =>
    (e \\ "datafield") find (df => df \@ "tag" == tag) map (df => if (indPos == 1) df \@ "ind1" else df \@ "ind2")

  /**
   * Return value of indicator. If field does not exist, return blank indicator
   */
  val getIndOrBlank: String => Int => Elem => Option[String] = tag => indPos => e => {
    getInd(tag)(indPos)(e) match {
      case oi@Some(_) => oi
      case None => Some(" ")
    }
  }
}

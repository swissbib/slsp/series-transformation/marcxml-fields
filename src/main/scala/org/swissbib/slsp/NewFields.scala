/*
 * Library to extract specific datafields from MARCXML records
 * Copyright (C) 2020  project swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.slsp

object NewFields {

  val field800: (String, String, String) => FieldBuilder = (sf839v, sf839w, sf8396) => FieldBuilder("800", IndSelectors.getInd("100")(1))
    .buildOnlyIfFieldExists("100")
    .addSubfield("6", sf8396)
    .registerSubfieldExtractor("100", "a")
    .registerSubfieldExtractor("100", "b")
    .registerSubfieldExtractor("100", "c")
    .registerSubfieldExtractor("100", "q")
    .registerSubfieldExtractor("100", "d")
    .registerSubfieldExtractor("100", "u")
    .registerSubfieldExtractor("100", "l")
    .registerSubfieldExtractor("240", "a", "t", "245", "a", "t")
    .registerSubfieldExtractor("240", "g", "245", "g")
    .registerSubfieldExtractor("240", "m")
    .registerSubfieldExtractor("240", "n", "245", "n")
    .registerSubfieldExtractor("240", "r")
    .registerSubfieldExtractor("240", "f", "245", "f")
    .registerSubfieldExtractor("240", "s", "245", "s")
    .registerSubfieldExtractor("240", "p", "245", "p")
    .registerSubfieldExtractor("240", "k", "245", "k")
    .registerSubfieldExtractor("240", "o")
    .addSubfield("v", sf839v)
    .addSubfield("w", sf839w)

  val field810: (String, String, String) => FieldBuilder = (sf839v, sf839w, sf8396) => FieldBuilder("810", IndSelectors.getInd("110")(1))
    .buildOnlyIfFieldExists("110")
    .addSubfield("6", sf8396)
    .registerSubfieldExtractor("110", "a")
    .registerSubfieldExtractor("110", "b")
    .registerSubfieldExtractor("110", "c")
    .registerSubfieldExtractor("110", "d")
    .registerSubfieldExtractor("110", "u")
    .registerSubfieldExtractor("110", "l")
    .registerSubfieldExtractor("240", "a", "t", "245", "a", "t")
    .registerSubfieldExtractor("240", "g", "245", "g")
    .registerSubfieldExtractor("240", "m")
    .registerSubfieldExtractor("240", "n", "245", "n")
    .registerSubfieldExtractor("240", "r")
    .registerSubfieldExtractor("240", "f", "245", "f")
    .registerSubfieldExtractor("240", "s", "245", "s")
    .registerSubfieldExtractor("240", "p", "245", "p")
    .registerSubfieldExtractor("240", "k", "245", "k")
    .registerSubfieldExtractor("240", "o")
    .addSubfield("v", sf839v)
    .addSubfield("w", sf839w)

  val field811: (String, String, String) => FieldBuilder = (sf839v, sf839w, sf8396) => FieldBuilder("811", IndSelectors.getInd("111")(1))
    .buildOnlyIfFieldExists("111")
    .addSubfield("6", sf8396)
    .registerSubfieldExtractor("111", "a")
    .registerSubfieldExtractor("111", "e")
    .registerSubfieldExtractor("111", "q")
    .registerSubfieldExtractor("111", "d")
    .registerSubfieldExtractor("111", "c")
    .registerSubfieldExtractor("111", "u")
    .registerSubfieldExtractor("111", "n")
    .registerSubfieldExtractor("111", "l")
    .registerSubfieldExtractor("240", "a", "t", "245", "a", "t")
    .registerSubfieldExtractor("240", "g", "245", "g")
    .registerSubfieldExtractor("240", "p", "245", "p")
    .registerSubfieldExtractor("240", "f", "245", "f")
    .registerSubfieldExtractor("240", "s", "245", "s")
    .registerSubfieldExtractor("240", "k", "245", "k")
    .addSubfield("v", sf839v)
    .addSubfield("w", sf839w)

  val field830: (String, String, String) => FieldBuilder = (sf839v, sf839w, sf8396) => FieldBuilder("830", IndSelectors.customInd(" "), Some(IndSelectors.customInd("0")))
    .buildOnlyIfFieldExists("245")
    .addSubfield("6", sf8396)
    .registerSubfieldExtractor("130", "a", "245", "a")
    .registerSubfieldExtractor("130", "d")
    .registerSubfieldExtractor("130", "g")
    .registerSubfieldExtractor("130", "m")
    .registerSubfieldExtractor("130", "n", "245", "n")
    .registerSubfieldExtractor("130", "r")
    .registerSubfieldExtractor("130", "l")
    .registerSubfieldExtractor("130", "f")
    .registerSubfieldExtractor("130", "s", "245", "s")
    .registerSubfieldExtractor("130", "p", "245", "p")
    .registerSubfieldExtractor("130", "k", "245", "k")
    .registerSubfieldExtractor("130", "o")
    .addSubfield("v", sf839v)
    .addSubfield("w", sf839w)
}
